# Set the base image to node:12-alpine
FROM node:16.15.1-alpine as build

# Specify where our app will live in the container
WORKDIR /app

# Copy the React App to the container
COPY . /app/

# Prepare the container for building React
RUN npm install
RUN npm install react-scripts@5.0.1 -g
# We want the production version
RUN npm run build

# Prepare nginx
FROM nginx:1.19.4-alpine

WORKDIR /usr/share/nginx/html

RUN rm -rf ./*

COPY --from=build /app/build /usr/share/nginx/html

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

# /etc/nginx/nginx.conf

# Fire up nginx
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]